# -*- mode: python ; coding: utf-8 -*-

block_cipher = None

SETUP_DIR = 'C:\\Users\\F1238618\\Desktop\\luping\\flask-python\\'
a = Analysis(['myflask.py','config.py','exts.py','model.py'],
             pathex=['C:\\Users\\F1238618\\Desktop\\luping\\flask-python'],
             binaries=[],
             datas=[(SETUP_DIR+'frontend','frontend')],
             hiddenimports=['pymysql'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='myflask',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='myflask')
